package com.game.businesshouse.engine;

import com.game.businesshouse.objects.CellType;

import java.util.Arrays;
import java.util.Iterator;
import java.util.stream.Collectors;

/**
 * The interface is meant to abstract away the designing of the Business Board game.
 * It is designed with the intent to provide to its clients an easy way to get the next Cell. The implementation's
 * of this interface are supposed to provide
 */
public interface Designer {
    CellType next();

    boolean hasNext();

    static Designer newInstance(String csv){
        //"J,H,L,H,E,L,H,L,H,J"
        Iterator<CellType> cellType = Arrays.stream(csv.split(",")).map(CellType::from)
                                            .collect(Collectors.toList())
                                            .iterator();
        return new Designer() {
            @Override
            public CellType next() {
                return cellType.next();
            }

            @Override
            public boolean hasNext() {
                return cellType.hasNext();
            }
        };

    }
}
