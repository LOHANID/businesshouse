package com.game.businesshouse.engine;

import com.game.businesshouse.objects.*;
import com.game.businesshouse.service.DefaultContextService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * The class represents an instance of a game.
 */
public class Game {
    private static final Logger logger = com.game.businesshouse.utils.Logger.get();
    private final ContextService service = DefaultContextService.instance();
    private final BusinessBoard board;
    private final Bank bank;
    private final GameContext context;
    private final Config config;
    private final String id;
    private final List<Player> players = new ArrayList<>();

    public Game(String cellSequence) {
        this.config = new Config() {
        };
        this.bank = Bank.newInstance(config);
        this.context = GameContext.newInstance();
        context.bank(bank);
        this.id = service.addContext(context);
        this.board = new BusinessBoard(id, config, Designer.newInstance(cellSequence));
    }

    public static void main(String... args) throws IOException {
        System.out.println("Welcome to Business Board!!!");
        Scanner in = new Scanner(System.in);

        while (true) {
            System.out.println("Press S for simulation, G for game, E for exit");
            String input = in.nextLine();
            if ("S".equalsIgnoreCase(input)) {
                try {
                    runSimulation();
                } catch (Exception e) {
                    logger.info("Caught Exception:" + e.getMessage());
                }
                continue;
            }
            if ("G".equalsIgnoreCase(input)) {
                runGame();
                continue;
            }
            if ("E".equalsIgnoreCase(input)) {
                System.out.println("Bye!!");
                System.exit(0);
            }
        }


    }

    private static void runGame() {
        Scanner in = new Scanner(System.in);
        String players = null;
        while (players == null || players.isEmpty()) {
            System.out.println("Enter names of player separated by ','");
            players = in.nextLine();
        }
        System.out.println("Enter cell sequence separated by ','. Example J,H,L,H,E,L,H,L,H,J. You can also press enter to use the default sequence");

        String sequence = in.nextLine();
        if (sequence.isEmpty()) {
            sequence = "J,H,L,H,E,L,H,L,H,J";
            System.out.println("Sequence:J,H,L,H,E,L,H,L,H,J ");
        }

        Game game = new Game(sequence);
        Arrays.stream(players.split(",")).forEach(game::addPlayer);
        Players ps = game.new Players(game.players());

        while (true) {
            Player player = ps.next();
            System.out.println("Enter to move [" + player.id() + "]");
            if (in.nextLine().isEmpty()) {
                player.move();
                System.out.println(player.id() + " moved to cell " + player.cell().id() + " of type " + player.cell().type());
                System.out.println(player.state());

            }
            if (checkExit(ps, game.config))
                break;
        }
        System.out.println(game.new Result().print());
        System.out.println("Winner:" + game.new Result().winner());

    }

    private static boolean checkExit(Players ps, Config config) {
        for (Player player : ps.players) {
            if (player.moves() <= config.playerMaxMoves())
                return false;
        }
        return true;
    }

    private static void runSimulation() throws IOException {
        Scanner in = new Scanner(System.in);
        System.out.println("Provide absolute file name to read input data");
        Properties props = new Properties();
        props.load(new ByteArrayInputStream(Files.readAllBytes(Paths.get(in.nextLine()))));
        Result result = new Game(props.getProperty("Cells"))
                .simulateFor(props.getProperty("Players")
                                  .split(","), props.getProperty("DiceOutput"));
        System.out.println(result.print());
        System.out.println("Winner:" + result.winner());

    }

    public List<Player> players() {
        return players;
    }

    public void addPlayer(String name) {
        players.add(Player.newInstance(this.board, this.config, name));
    }

    /**
     * @param players
     * @param diceRollsCsv
     * @return Runs a simulation for the game for the given players and given dice roll sequence
     */
    public Result simulateFor(String[] players, String diceRollsCsv) {
        Arrays.stream(players).forEach(this::addPlayer);
        Players ps = new Players(this.players);
        Arrays.stream(diceRollsCsv.split(",")).forEach(s -> ps.next().move(Integer.parseInt(s.trim())));
        return new Result();
    }

    /**
     * The class encapsulates the results of the game. Provides an easy way to query the state parameters of the result of the game (or simulation)
     */
    public class Result {

        public String print() {
            StringBuilder builder = new StringBuilder();
            players.forEach(player -> builder.append("\n").append(player.state()));
            builder.append("\n").append(bank.state());
            return builder.toString();
        }

        public double balance(String playerId) throws Exception {
            return players.stream().filter(player -> playerId.equals(player.id())).findFirst().get().balance();
        }

        public double asset(String playerId) throws Exception {
            return players.stream().filter(player -> playerId.equals(player.id()))
                          .findFirst().get().assets().stream().mapToDouble(value -> value.value()).sum();
        }

        public double bankBalance() {
            return bank.balance();
        }

        /**
         * @return The winner(s) of the game. If two or more players are tried for the maximum balance,
         * both are assumed to be winners!. Asset value is not taken into consideration.
         * The winner(s) can be determined at any point in the game.
         */
        public String winner() {
            Player winner = players.get(0);
            for (Player player : players) {
                if (player.balance() > winner.balance())
                    winner = player;
            }
            Player finalWinner = winner;
            return players.stream().filter(player -> player.balance() == finalWinner.balance()).map(Player::id)
                          .collect(Collectors.joining(","));
        }
    }

    /**
     * A utility class to wrap a player list and provide the next player in a circular fashion
     */
    private class Players {
        private final List<Player> players;
        private int index;

        private Players(List<Player> players) {
            this.players = players;
            this.index = 0;
        }

        /**
         * @return the method returns the next player ina a circular fashion.
         */
        private Player next() {
            if (index >= players.size())
                index = players.size() - index;
            Player player = players.get(index);
            index++;
            context.currentPlayer(player);
            return player;
        }
    }
}
