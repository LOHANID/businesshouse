package com.game.businesshouse.objects;

import com.game.businesshouse.service.DefaultContextService;

import java.util.logging.Logger;

/**
 * Represents a Cell marked as Hotel.
 */
public class HotelCell implements Cell, Asset {
    private final ContextService ctxService = DefaultContextService.instance();
    private final Logger logger = com.game.businesshouse.utils.Logger.get();
    private final String contextId;
    private final String id;
    private HotelType type;
    private Player owner;

    public HotelCell(String contextId, String id, Config config) {
        this.contextId = contextId;
        this.id = id;
        this.type = HotelType.Silver;
    }

    @Override
    public CellType type() {
        return CellType.Hotel;
    }

    @Override
    public void onLand() {
        if (ctxService.context(contextId) == null) {
            logger.info("Empty Context, Can't proceed");
            return;
        }
        /*When the player lands on it and has required money, he has to buy it by paying the bank
        the required money to buy a silver hotel.*/
        Player player = ctxService.context(contextId).currentPlayer();
        if (!player.owns(this) && player.balance() >= HotelType.Silver.value() && this.owner == null) {
            //make player the owner
            buy();
            return;
        }
        /*If the player lands on it’s pre-owned hotel and has required money, the player needs to
            upgrade hotel by paying required delta value.
            Silver to Gold -> 100
            Gold to Platinum -> 200*/
        if (player.owns(this) && player.balance() >= type.upgradeCost()) {
            upgrade();
            return;
        }
        /*If any other player lands on a pre-owned hotel, the player needs to pay rent as per hotel
           state (Silver, Gold, Platinum) to hotel owner.*/
        if (!player.owns(this) && this.owner != null) {
            payRent();
        }

    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public String state() {
        return "id:" + id() + "-Type:" + type() + (owner() != null ? "-owner:"+owner().id():"");
    }

    private void payRent() {
        Player player = ctxService.context(contextId).currentPlayer();
        //Debit from current player
        player.debit(type.rent());
        //Credit to owner
        this.owner().credit(type.rent());
    }

    private void upgrade() {
        Player player = ctxService.context(contextId).currentPlayer();
        Bank bank = ctxService.context(contextId).bank();
        player.debit(type.upgradeCost());
        bank.credit(type.upgradeCost());
        type = type.upgrade();
    }

    private void buy() {
        Player player = ctxService.context(contextId).currentPlayer();
        Bank bank = ctxService.context(contextId).bank();
        player.debit(HotelType.Silver.value());
        bank.credit(HotelType.Silver.value());
        this.owner = player;
        player.own(this);
    }

    @Override
    public Player owner() {
        return owner;
    }

    @Override
    public double value() {
        return type.value();
    }
}
