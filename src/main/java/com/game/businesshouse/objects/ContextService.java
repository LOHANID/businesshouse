package com.game.businesshouse.objects;

public interface ContextService {

    GameContext context(String contextId);

    String addContext(GameContext context);

    void removeContext(String id);
}
