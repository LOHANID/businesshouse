package com.game.businesshouse.objects;

public interface Asset {
    Player owner();
    double value();
}
