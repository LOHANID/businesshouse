package com.game.businesshouse.objects;

import java.util.List;

public interface Board {
    Cell movePlayer(Player player, int steps);

    Cell startingCell();

    default String state (){
        StringBuilder builder = new StringBuilder();
        cells().forEach(cell -> builder.append("\n").append(cell.state()));
        return builder.toString();
    }

    List<Cell> cells ();
}
