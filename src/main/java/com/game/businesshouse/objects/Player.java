package com.game.businesshouse.objects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

/**
 * This interface represents a player.
 */
public interface Player {
    static Player newInstance(Board board, Config config, String playerName) {
        return new Player() {
            private final Logger logger = com.game.businesshouse.utils.Logger.get();
            private final List<Asset> assets = new ArrayList<>();
            private double balance = config.initialAmountWithPlayer();
            private Cell cell = null;
            private int move = 0;

            @Override
            public void debit(double amount) {
                balance = balance - amount;
            }

            @Override
            public void credit(double amount) {
                balance = balance + amount;
            }

            @Override
            public double balance() {
                return balance;
            }

            @Override
            public void move() {
                //we don't want 0 to be the number of moves, so select a bound 1 less then 13 and add 1
                move(new Random().nextInt(12) + 1);
            }

            @Override
            public Cell cell() {
                return cell;
            }

            @Override
            public String id() {
                return playerName;
            }

            @Override
            public void move(int steps) {
                if (move <= config.playerMaxMoves()) {
                    move++;
                }
                else{
                    logger.info("Moves completed for player:" + id());
                    return;
                }
                this.cell = board.movePlayer(this, steps);
                this.cell.onLand();
                logger.info("Player-" + id() + " moved " + steps + " steps to " + this.cell.id() + " of type " + this.cell.type());
                logger.info("State after the move->" + this.state());
                //logger.info("Board State->" + board.state());
            }

            @Override
            public void own(Asset asset) {
                assets.add(asset);
            }

            @Override
            public List<Asset> assets() {
                return assets;
            }

            /**
             * @return Number of moves mae by player so far
             */
            @Override
            public int moves() {
                return move;
            }
        };

    }

    /**
     * @param amount
     * Debits the amount from players balance.
     */
    void debit(double amount);

    /**
     * @param reward
     * Credits the reward to players balance
     */
    void credit(double reward);

    /**
     * @return
     * The current balance available with the Player
     */
    double balance();

    /**
     * @param asset
     * @return whether the player owns the given {@link Asset} or not
     */
    default boolean owns(Asset asset) {
        return this.equals(asset.owner());
    }

    /**
     * The action that represents player rolling a dice and making a move as per the roll results.
     */
    void move();

    /**
     * @return
     * The current cell the player is occupying
     */
    Cell cell();

    /**
     * @return
     * Unique Player id
     */
    String id();

    /**
     * @param steps
     * Moves the player given number of steps in the board
     */
    void move(int steps);

    /**
     * @return
     * Describes the state of the player
     */
    default String state() {
        return "Player-" + id() + " has total money " + balance() + " and asset of amount : " + assets().stream()
                                                                                                        .map(Asset::value)
                                                                                                        .mapToDouble(value -> value).sum();
    }

    /**
     * @param asset
     * Action describing player owning the given asset
     */
    void own(Asset asset);

    /**
     * @return
     * Assets owned by the player
     */
    List<Asset> assets();

    /**
     * @return
     * Number of moves mae by player so far
     */
    int moves();
}
