package com.game.businesshouse.objects;

public interface Bank {
    static Bank newInstance(Config config) {
        return new Bank() {
            double amount = config.initialAmountWithBank();

            @Override
            public void credit(double credit) {
                this.amount = this.amount + credit;
            }

            @Override
            public void debit(double debit) {
                this.amount = this.amount - debit;
            }

            @Override
            public double balance() {
                return amount;
            }
        };
    }

    void credit(double penalty);

    void debit(double reward);

    double balance();

    default String state() {
        return "Balance at Bank :" + balance();
    }
}
