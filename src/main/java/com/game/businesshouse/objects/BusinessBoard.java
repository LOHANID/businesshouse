package com.game.businesshouse.objects;

import com.game.businesshouse.engine.Designer;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a Business Board
 */
public class BusinessBoard implements Board {
    private final Config config;
    private final List<Cell> cells = new ArrayList<>();
    private final String contextId;

    public BusinessBoard(String id, Config config, Designer designer) {
        this.config = config;
        this.contextId = id;
        initialize(designer);
    }

    private void initialize(Designer designer) {
        int i = 1;
        while (designer.hasNext()) {
            cells.add(Cell.newInstance(contextId, "cell." + i, config, designer.next()));
            i++;
        }
    }

    @Override
    public Cell movePlayer(Player player, int steps) {
        if (player.cell() == null) {
            //first move for player
            if (steps >= cells.size())
                steps = steps%cells.size();
            return cells.get(steps - 1);
        }
        int nextLocation = cells.indexOf(player.cell()) + steps;
        //Cells arranged in circular fashion, reduce in multiples of cell size
        if (nextLocation >= cells.size())
            nextLocation = nextLocation%cells.size();
        return cells.get(nextLocation);
    }

    @Override
    public Cell startingCell() {
        return cells.get(0);
    }

    @Override
    public List<Cell> cells() {
        return this.cells;
    }


}
