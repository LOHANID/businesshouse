package com.game.businesshouse.objects;

import com.game.businesshouse.service.DefaultContextService;

public class LotteryCell implements Cell {

    private final ContextService ctxService = DefaultContextService.instance();
    private final double reward;
    private final String id;
    private final String contextId;

    public LotteryCell(String contextId, String id, Config config) {
        this.contextId=contextId;
        this.id = id;
        this.reward = config.lotteryReward();
    }

    @Override
    public CellType type() {
        return CellType.Lottery;
    }

    @Override
    public void onLand() {
        ctxService.context(contextId).currentPlayer().credit(reward);
        ctxService.context(contextId).bank().debit(reward);
    }

    @Override
    public String id() {
        return id;
    }
}
