package com.game.businesshouse.objects;

/**
 * A general config interface that can have various implementations
 * allowing the configurations of the game to be loaded from a variety of sources (for example, from DB, or File, or  a REST API)
 * Provides a default implementation with defaults
 */
public interface Config {
    default double jailPenalty() {
        return 150;
    }

    default double lotteryReward() {
        return 200;
    }

    default double initialAmountWithBank() {
        return 5000;
    }

    default double initialAmountWithPlayer() {
        return 1000;
    }

    default int playerMaxMoves() {
        return 10;
    }
}
