package com.game.businesshouse.objects;

/**
 * The interface defines the behavior/parameters of a BusinessBoard Cell.
 */
public interface Cell {
    /**
     * @param contextId
     * @param id
     * @param config
     * @param type
     * @return an instance of the Cell created using the provided information
     */
    static Cell newInstance(String contextId, String id, Config config, CellType type) {
        switch (type) {
            case Hotel:
                return new HotelCell(contextId, id, config);
            case Lottery:
                return new LotteryCell(contextId, id, config);
            case Jail:
                return new JailCell(contextId, id, config);
            default:
                return () -> id;
        }
    }

    default CellType type() {
        return CellType.Empty;
    }

    /**
     * The method that must be invoked on the Cell when a player lands on it. The default implementation describes hat must happen if the player lands on an empty {@link Cell}
     */
    default void onLand() {
        //Do nothing. The default implementation corresponds to an empty cell
    }

    /**
     * @return
     * Unique ID of the Cell
     */
    String id();

    /**
     * @return
     * State of the Cell
     */
    default String state() {
        return "id:" + id() + "-Type:" + type();
    }
}

