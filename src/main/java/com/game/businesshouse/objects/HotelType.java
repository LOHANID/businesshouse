package com.game.businesshouse.objects;

public enum HotelType {
    Silver(200, 50), Gold(300, 150), Platinum(500, 300);

    private final int value;
    private final int rent;

    HotelType(int value, int rent) {
        this.value = value;
        this.rent = rent;
    }

    public int value() {
        return value;
    }

    public int rent() {
        return rent;
    }

    public int upgradeCost() {
        if (this.equals(HotelType.Silver))
            return 100;
        if (this.equals(HotelType.Gold))
            return 200;
        return 0;
    }

    public HotelType upgrade() {
        if (this.equals(HotelType.Silver))
            return Gold;
        if (this.equals(HotelType.Gold))
            return Platinum;
        return Platinum;
    }
}
