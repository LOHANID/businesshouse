package com.game.businesshouse.objects;


public interface GameContext {
    static GameContext newInstance() {
        return new GameContext() {
            private Player player;
            private Bank bank;

            @Override
            public Player currentPlayer() {
                return player;
            }

            @Override
            public Bank bank() {
                return bank;
            }

            @Override
            public void bank(Bank bank) {
                this.bank = bank;
            }

            @Override
            public void currentPlayer(Player player) {
                this.player = player;
            }


        };
    }

    Player currentPlayer();

    Bank bank();

    void bank(Bank bank);

    void currentPlayer(Player player);
}
