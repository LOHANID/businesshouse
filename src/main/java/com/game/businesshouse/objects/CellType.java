package com.game.businesshouse.objects;

public enum CellType {
    Jail,Lottery,Hotel,Empty;

    public static CellType from(String s){
        if ("J".equals(s))
            return Jail;
        if ("L".equals(s))
            return Lottery;
        if ("H".equals(s))
            return Hotel;
        if ("E".equals(s))
            return Empty;

        return Empty;
    }
}
