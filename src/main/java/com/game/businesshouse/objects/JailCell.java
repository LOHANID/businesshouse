package com.game.businesshouse.objects;

import com.game.businesshouse.service.DefaultContextService;

public class JailCell implements Cell {


    private final ContextService ctxService = DefaultContextService.instance();
    private final double penalty;

    private final String id;
    private final String contextId;

    public JailCell(String contextId, String id, Config config) {
        this.contextId=contextId;
        this.id = id;
        this.penalty = config.jailPenalty();
    }

    @Override
    public CellType type() {
        return CellType.Jail;
    }

    @Override
    public void onLand() {
        ctxService.context(contextId).currentPlayer().debit(penalty);
        ctxService.context(contextId).bank().credit(penalty);
    }

    @Override
    public String id() {
        return id;
    }
}
