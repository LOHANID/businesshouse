package com.game.businesshouse.service;

import com.game.businesshouse.objects.ContextService;
import com.game.businesshouse.objects.GameContext;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class DefaultContextService implements ContextService {
    private static final ContextService instance = new DefaultContextService();
    private final Map<String, GameContext> ctxMap = new HashMap<>();

    public static ContextService instance() {
        return instance;
    }

    @Override
    public GameContext context(String contextId) {
        return ctxMap.get(contextId);
    }

    @Override
    public String addContext(GameContext context) {
        String id = new Random().nextDouble() + "";
        ctxMap.putIfAbsent(id, context);
        return id;
    }

    @Override
    public void removeContext(String id) {
        ctxMap.remove(id);
    }
}
