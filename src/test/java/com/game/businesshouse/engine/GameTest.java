package com.game.businesshouse.engine;

import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class GameTest {
    private final Game game = new Game("J,H,L,H,E,L,H,L,H,J");

    @Test
    public void simulateForIS1() throws Exception {
        Game.Result result = game.simulateFor(new String []{"1","2","3"},"2,2,1,4,4,2,4,4,2,2,2,1,4,4,2,4,4,2,2,2,1");
        System.out.println(result.print());

        Assert.assertEquals(result.balance("1"),1100.0,0.0);
        Assert.assertEquals(result.asset("1"),500.0,0.0);

        Assert.assertEquals(result.balance("2"),600.0,0.0);
        Assert.assertEquals(result.asset("2"),0.0,0.0);

        Assert.assertEquals(result.balance("3"),1150,0.0);
        Assert.assertEquals(result.asset("2"),0.0,0.0);

        Assert.assertEquals(result.bankBalance(),5150.0,0.0);
        Assert.assertEquals(result.winner(),"3");
    }

    @Test
    public void simulateForIS2() throws Exception {
        Game.Result result = game.simulateFor(new String []{"1","2","3"},"2,2,1, 4,2,3, 4,1,3, 2,2,7, 4,7,2, 4,4,2, 2,2,2");
        System.out.println(result.print());

        Assert.assertEquals(result.balance("1"),650.0,0.0);
        Assert.assertEquals(result.asset("1"),500.0,0.0);

        Assert.assertEquals(result.balance("2"),750.0,0.0);
        Assert.assertEquals(result.asset("2"),300.0,0.0);

        Assert.assertEquals(result.balance("3"),850.0,0.0);
        Assert.assertEquals(result.asset("3"),200.0,0.0);

        Assert.assertEquals(result.bankBalance(),5750.0,0.0);

        Assert.assertEquals(result.winner(),"3");
    }

    @Test
    public void simulateForIS1UsingFile() throws Exception {
        String simulationInputFile = "src/test/resources/inputSample.properties";
        Properties props = new Properties();
        props.load(new ByteArrayInputStream(Files.readAllBytes(Paths.get(simulationInputFile))));
        Game.Result result = new Game(props.getProperty("Cells"))
                .simulateFor(props.getProperty("Players")
                                  .split(","), props.getProperty("DiceOutput"));

        System.out.println(result.print());


        Assert.assertEquals(result.balance("1"),1100.0,0.0);
        Assert.assertEquals(result.asset("1"),500.0,0.0);

        Assert.assertEquals(result.balance("2"),600.0,0.0);
        Assert.assertEquals(result.asset("2"),0.0,0.0);

        Assert.assertEquals(result.balance("3"),1150,0.0);
        Assert.assertEquals(result.asset("2"),0.0,0.0);

        Assert.assertEquals(result.bankBalance(),5150.0,0.0);
        Assert.assertEquals(result.winner(),"3");

    }

}