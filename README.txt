1) The main class for the game can be located at com.game.businesshouse.engine.Game#main
2) The game can be run in two modes 1) Simulation (S) and 2) Play (G). On start the two options are presented to the user in console.
3) The property file format for running the simulation is
	###################
	Cells=J,H,L,H,E,L,H,L,H,J
	DiceOutput = 2,2,1, 4,4,2, 4,4,2, 2,2,1, 4,4,2, 4,4,2, 2,2,1
	Players=1,2,3
	###################
4) The simulation results are printed to the console (System.out) in the following format
	###################
	Player-1 has total money 1100.0 and asset of amount : 500.0
	Player-2 has total money 600.0 and asset of amount : 0.0
	Player-3 has total money 1150.0 and asset of amount : 0.0
	Balance at Bank :5150.0
	Winner:3
	####################
5) Major assumptions are
	a) If the simulation input file contains moves in excess of 10 for a player, the additional moves will be ignored
	b) The asset value does not count towards winnings of the user, which implies assets would be ignored while determining the winner
	c) If two (or more) players tie for the maximum balance, both will be considered winners
	d) If the first dice roll results into a value greater than the number of "cells", the number of steps is calculated as the excess amount. For example for 6 cells if the dice roll results in 7, the player will be moved to the 1st cell.
	